#language: es
#encoding: utf-8

@US-001
Característica: Ver el detalle de un Request de PPM de entre una lista de Request asignados
	Yo como Collaborador de Softtek
	Necesito necesito buscar y ver el detalle de un Request abierto asignado a mi de entre una lista de Requests
	De manera que pueda atenderlo

@TestLink
@BI-1
Escenario: Busqueda exitosa de Requests asignado
	Dado que ya estoy firmado
		Y puedo ver el titulo de "Mis asignaciones"
		Y puedo accesar a la página de búsqueda de solicitudes
	Cuando escribo el Tipo de Request que busco
		Y pido hacer una Búsqueda Avanzada
		Y selecciono el nombre del responsable Asignado del Request
		Y le pido que realice la busqueda
	Entonces Yo espero ver el listado de los Request asociados y seleccionar uno
		Y Ver el detalle del Request seleccionado