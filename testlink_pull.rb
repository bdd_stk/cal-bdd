require 'json'
require 'yaml'
require 'xmlrpc/client'
require 'nokogiri'
require 'pp'
require 'active_record'

config = YAML.load_file('testlink.yml')

ActiveRecord::Base.establish_connection(  
  :adapter  => "mysql",
  :host     => config['dbTestLink'],
  :port     => config['dbPortTestLink'],
  :username => "config['testlinkUser']",
  :password => "config['testLinkPassword']",
  :database => "config['testLinkDB']"
)

server = XMLRPC::Client.new2 config['url']

params = { devKey: config['devKey'], testprojectname: config['projectName'] }
project = server.call("tl.getTestProjectByName", params)
#pp project

if project['active'] == '1'

  #inTestPlan = config['testPlans'].map(&:inspect).join(',')
  inTestPlan = config['testPlanIds'].join(',')

  sql1 = "select TP.testproject_id, TP.id as testplan_id, RC.req_id from nodes_hierarchy NHV
      left join nodes_hierarchy NH on NH.id=NHV.parent_id
      left join req_coverage RC on RC.testcase_id=NH.id
      left join testplan_tcversions TPTC on TPTC.tcversion_id=NHV.id
      left join testplans TP on TP.id=TPTC.testplan_id
      left join tcversions TCV on TCV.id=NHV.id
      where NHV.node_type_id=4 and TP.active=1 and TP.is_public=1 and TCV.`status`=7 and TCV.execution_type=2 and TP.testproject_id=#{project['id']} and TP.id in (#{inTestPlan})
      group by TP.testproject_id, TP.id, RC.req_id
      order by TP.testproject_id, TP.id, RC.req_id"

  reqByTestPlan =  ActiveRecord::Base.connection.select_all sql1
  reqByTestPlan.each { |req|
    #pp req

    sql2 = "select NH.id as req_id, R.req_doc_id as req_code, NH.name as req_name, RV.scope as req_desc, RV.`status` as req_status, RV.`type` as req_type, RV.active as req_active, RV.expected_coverage as req_expected_coverage, srs.id as req_parent_id, srs.name as req_parent_name from nodes_hierarchy N
        left join nodes_hierarchy NH on NH.id=N.parent_id
        left join requirements R on R.id=NH.id
        left join req_versions RV on RV.id=N.id
        left join nodes_hierarchy srs on srs.id=NH.parent_id
        where N.node_type_id=8 and NH.id=#{req['req_id']}"
    feature =  ActiveRecord::Base.connection.select_one sql2
    #pp feature
    feature_desc = """#language: es
#encoding: utf-8
@#{feature['req_code']}
Característica: #{feature['req_name']}
#{Nokogiri::HTML(feature['req_desc']).text.gsub(/\r\n\r\n?/, "\r\n")}
"""

    sql3 = "select TP.id as testplan_id, RC.req_id as req_id, NH.id as tc_id, TCV.tc_external_id as tc_external_id, CONCAT_WS('-', P.prefix, TCV.tc_external_id) as tc_external_code, TCV.`status` as tc_status, NH.name as tc_name, TCV.execution_type as tc_execution_type, TCV.summary as tc_description, TCV.preconditions as tc_source_code, NH.parent_id as tc_parent_id, TPTC.node_order as tc_seq, TPTC.platform_id as platform_id
        from nodes_hierarchy NHV
        left join nodes_hierarchy NH on NH.id=NHV.parent_id
        left join req_coverage RC on RC.testcase_id=NH.id
        left join testplan_tcversions TPTC on TPTC.tcversion_id=NHV.id
        left join testplans TP on TP.id=TPTC.testplan_id
        left join tcversions TCV on TCV.id=NHV.id
        left join testprojects P on P.id=TP.testproject_id
        where NHV.node_type_id=4 and TP.active=1 and TP.is_public=1 and TCV.`status`=7 and TCV.execution_type=2 and RC.req_id=#{req['req_id']}
        order by TP.id, NH.node_order"
    #puts sql3
    activeTestCases = ActiveRecord::Base.connection.select_all sql3
    activeTestCases.each { |tc|
      #pp tc

      scenario_desc = """
@TestLink
@#{tc['tc_external_code']}
Escenario: #{tc['tc_name']}
#{Nokogiri::HTML(tc['tc_description']).text.gsub(' ', '')}
"""

      feature_desc += scenario_desc
    }

    #puts feature_desc
    File.write('features/testlink/'+feature['req_code']+'.feature', feature_desc)
  }
else
  puts "Project is not active, it can't execute any Test Case."
end