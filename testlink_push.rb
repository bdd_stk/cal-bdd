require 'json'
require 'yaml'
require 'xmlrpc/client'
require 'nokogiri'
require 'pp'
require 'active_record'
require 'savon'

ActiveRecord::Base.establish_connection(  
  :adapter  => "mysql",
  :host     => config['dbTestLink'],
  :port     => config['dbPortTestLink'],
  :username => "config['testlinkUser']",
  :password => "config['testLinkPassword']",
  :database => "config['testLinkDB']"
)

config = YAML.load_file('testlink.yml')
server = XMLRPC::Client.new2 config['url']

config_mantis = YAML.load_file('mantis.yml')
client_mantis = Savon.client(wsdl: config_mantis['url'])
#pp client_mantis.operations

output_file = File.read("cucumber-output.json")
data_hash = JSON.parse(output_file)
#pp data_hash
data_hash.each { |feature|
  feature_code = feature['tags'][0]['name'].gsub('@', '')
  puts feature['keyword'] + ': ' + feature['name']
  feature['elements'].each { |scenario|
    tc_external_code = scenario['tags'][1]['name'].gsub('@', '')
    puts '    ' + scenario['keyword'] + ': ' + scenario['name'] + ' ' + tc_external_code

    sql3 = "select TP.id as testplan_id, RC.req_id as req_id, NH.id as tc_id, TCV.tc_external_id as tc_external_id, CONCAT_WS('-', P.prefix, TCV.tc_external_id) as tc_external_code, TCV.`status` as tc_status, NH.name as tc_name, TCV.execution_type as tc_execution_type, TCV.summary as tc_description, TCV.preconditions as tc_source_code, NH.parent_id as tc_parent_id, TPTC.node_order as tc_seq, TPTC.platform_id as platform_id
        from nodes_hierarchy NHV
        left join nodes_hierarchy NH on NH.id=NHV.parent_id
        left join req_coverage RC on RC.testcase_id=NH.id
        left join testplan_tcversions TPTC on TPTC.tcversion_id=NHV.id
        left join testplans TP on TP.id=TPTC.testplan_id
        left join tcversions TCV on TCV.id=NHV.id
        left join testprojects P on P.id=TP.testproject_id
        where NHV.node_type_id=4 and TP.active=1 and TP.is_public=1 and TCV.`status`=7 and TCV.execution_type=2 and CONCAT_WS('-', P.prefix, TCV.tc_external_id)='#{tc_external_code}'
        order by TP.id, NH.node_order"
    #puts sql3
    testCase = ActiveRecord::Base.connection.select_one sql3
    #pp testCase

    params = { devKey: config['devKey'], testplanid: testCase['testplan_id'] }
    latest_build = server.call("tl.getLatestBuildForTestPlan", params)
    #pp latest_build

    execution_success = true
    execution_errors = ''
    execution_duration = 0
    issue_summary = ''
    issue_description = ''
    issue_steps = ''
    scenario['steps'].each { |step|
      if step['result']['status'] != 'passed'
        execution_success = false
        execution_errors += (step['match']['location'] + "\r\n")
      end
      puts '        ' + step['keyword'] + ': ' + step['name'] + ' (' + step['result']['status'] + ') ' + "<<#{execution_success}>>"
      execution_duration += step['result']['duration']
    }

    if execution_success
      execution_status = 'p'
      execution_message = "Successful execution!\r\n\r\n"
    else
      execution_status = 'f'
      execution_message = "Errors found in:\r\n" + execution_errors + "\r\n\r\n"
      issue_summary = "Errors found in: features/testlink/#{feature_code}.feature (Scenario: #{tc_external_code})"
      issue_description = execution_message
      feature_definition = File.read("features/testlink/#{feature_code}.feature")
      feature_code = File.read("features/step_definitions/testlink/#{feature_code}_steps.rb")
      issue_steps = "#{feature_definition}\r\n\r\n#{feature_code}"
      #pp issue_steps
      execution_message += feature_code
    end

    # Create a Bug in Mantis
    bug_id = 0
    if execution_status == 'f'
      issue_data = [
        project: {name: config_mantis['projectName']},
        category: 'General',
        reporter: {name: 'testlink'},
        summary: issue_summary,
        description: "#{issue_description}\r\n\r\n#{issue_steps}",
        steps_to_reproduce: "#{issue_steps}"
      ]
      params = {username: config_mantis['username'], password: config_mantis['password'], issue: issue_data}
      new_issue = client_mantis.call(:mc_issue_add, message: params)
      issue_id = new_issue.xpath("//return/text()")
      bug_id = issue_id.to_s
      #pp bug_id
    end


    params = { devKey: config['devKey'],
               testcaseid: testCase['tc_id'],
               testcaseexternalid: testCase['tc_external_id'],
               testplanid: testCase['testplan_id'],
               buildid: latest_build['id'],
               status: execution_status,
               notes: execution_message,
               platformid: testCase['platform_id'],
               overwrite: false,
               bugid: bug_id
             }
    execresult = server.call("tl.setTestCaseExecutionResult", params)
    #pp execresult

    params = { devKey: config['devKey'], testplanid: testCase['testplan_id'], testcaseid: testCase['tc_id'] }
    last_execution_result = server.call("tl.getLastExecutionResult", params)
    pp last_execution_result

    #puts execution_duration
    execution_duration = (execution_duration.to_f/1000000000)/60
    #pp execution_duration
    sql = "update executions set execution_duration=#{execution_duration} where id=#{last_execution_result[0]['id']}"
    st = ActiveRecord::Base.connection.raw_connection.prepare(sql)
    st.execute

  }
}